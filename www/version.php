<?php 
$OC_Version = array(11,0,3,2);
$OC_VersionString = '11.0.3';
$OC_Edition = '';
$OC_Channel = 'stable';
$OC_VersionCanBeUpgradedFrom = array (
  'nextcloud' => 
  array (
    '9.1' => true,
    '11.0' => true,
  ),
  'owncloud' => 
  array (
    '9.1' => true,
  ),
);
$OC_Build = '2017-04-22T06:28:24+00:00 9649661f8fc0220b837975fe8afef5b5f7a964ec';
$vendor = 'nextcloud';
